from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'tango_with_django_project.views.home', name='home'),
    # url(r'^tango_with_django_project/', include('tango_with_django_project.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^app_WYSIWYG/', include('app_WYSIWYG.urls')),
    url(r'^app_WYSIWYG_editor/', include('app_WYSIWYG_editor.urls')),
    url(r'^app_WYSIWYG_presentation/', include('app_WYSIWYG_presentation.urls')),
    url(r'^app_WYSIWYG_adiestramiento/', include('app_WYSIWYG_adiestramiento.urls')),

)
      