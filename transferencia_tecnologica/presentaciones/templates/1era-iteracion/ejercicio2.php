<!DOCTYPE html>

<html class="no-js" lang="es">
<head>
    <title>Agilidad en el Contexto de RRHH - PDVSA AIT</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
<meta name="viewport" content="width=800, user-scalable=no">





</head>



    <body class="impress-not-supported" >

<?php include '../includes/css-js.html';?>








<!----------------- SLIDE #1 ------------------>




                    <img src="../../static/ref/images/playme1.png" width="370" height="170">


                    <h1><time>00:00:00</time></h1>
                    <button id="start">start</button>
                    <button id="stop">stop</button>
                    <button id="clear">clear</button>

                    <script>
                    var h1 = document.getElementsByTagName('h1')[0],
                    start = document.getElementById('start'),
                    stop = document.getElementById('stop'),
                    clear = document.getElementById('clear'),
                    seconds = 0, minutes = 0, hours = 0,
                    t;

                    function add() {
                      seconds++;
                      if (seconds >= 60) {
                        seconds = 0;
                        minutes++;
                        if (minutes >= 60) {
                          minutes = 0;
                          hours++;
                        }
                      }

                      h1.textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);

                      timer();
                      }
                      function timer() {
                          t = setTimeout(add, 1000);
                        }
                        timer();


                        /* Start button */
                        start.onclick = timer;

                        /* Stop button */
                        stop.onclick = function() {
                          clearTimeout(t);
                        }

                        /* Clear button */
                        clear.onclick = function() {
                          h1.textContent = "00:00:00";
                          seconds = 0; minutes = 0; hours = 0;
                        }
                        </script>





</body>
</html>
