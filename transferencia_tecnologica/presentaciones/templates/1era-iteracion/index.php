<!DOCTYPE html>

<html class="no-js" lang="es">
<head>
    <title>1. Un acercamiento a la transformación digital</title>
     <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
<meta name="viewport" content="width=800, user-scalable=no">





</head>



    <body class="impress-not-supported" >

<?php include '../includes/css-js.html';?>
<?php include '../includes/header_presentation.html';?>
<!--<script src="easytimer.min.js"></script>-->




<div id="impress">

  <div id="CVP" class="step" data-x="1000" data-y="2000" data-scale="2"><br>
    <span class="fonts_title_presentation_main3">
        Banplus<br>
    </span>
    <span class="datosOnofre">
      <ol>
    <li> </li>
    <li><br>A través de:<br>
      <b>La Gerencia de Capacitación y Desarrollo
      <br>
      Vicepresidencia Ejecutiva de Capital Humano</b></li>

    </ol>

    </span>
        <br>



    </div>


    <div id="titulo" class="step" data-x="1200" data-y="2300" data-z="-100" data-rotate-x="-40" data-rotate-y="10" data-scale="2">
        <span class="fonts_title_presentation_main">Un acercamiento a la <br>
        transformación digital
        </span>

    </div>


<!----------------- SLIDE #1 ------------------>

   <div class="step" data-x="1000" data-y="1000" data-scale="4.5">

        <span class="fonts_title_presentation_main2">
            Onofre Liscano<br>
        </span>
        <span class="datosOnofre">


        </span>
            <br>

            <table style="width:80%">

                <tr>
                    <td><a href="https://www.facebook.com/onofreliscano" target="_blank"><img src="../../static/ref/images/logo-fb.jpg" width="40" height="40"></a></td>
                    <td><a href="https://twitter.com/onofreliscano" target="_blank"><img src="../../static/ref/images/logo-tw.jpg" width="40" height="40"></a></td>
                    <td><a href="https://ve.linkedin.com/in/onofre-liscano-95189319" target="_blank"><img src="../../static/ref/images/logo-in.jpg" width="40" height="40"></a></td>
                    <td><a href="https://plus.google.com/+onofreliscano" target="_blank"><img src="../../static/ref/images/logo-g.jpg" width="40" height="40"></a></td>
                    <td><span class="datosOnofreSocial">@onofreliscano</span></td>
                </tr>

              </table>




    </div>

<!----------------- SLIDE #2 ------------------>
<!--<a href="../../static/downloads/Presentacion_SIGRH.pdf" class="fonts_link_slide_pdvsa" target="_blank">Descargar presentación en formato PDF</a>-->
<!--
    <div class="step" data-x="0" data-y="400" data-scale="0.5" data-rotate="90" >
        <center>
        <span class="fonts_title_presentation_main">
             Plan de Jubilación. II Semestre 2017
             CVP y Empresas Mixtas<br>
             Gerencia de RRHH CVP
        </span><br>

        </center>
    </div>-->

<div class="step" data-x="0" data-y="400" data-scale="1" data-rotate="90" >
  <span class="fonts_title_presentation_main3">
      Esta presentación se encuentra disponible en:<br>
  </span>
  <span class="datosOnofre">
    <ol>
<li>Gitlab<br><a href="https://onofreliscano.gitlab.io/banplus" target="_blank" class="fonts_main_links">https://onofreliscano.gitlab.io/banplus</a></li><br>

</ol>

  </span>
    </div>

<!----------------- SLIDE #0 ------------------>

    <div id = "slide0" class="step slide_PDVSA" data-x="0" data-y="-1500">
        <p align="left">
        <br><br><br>
        <br><br>
        <span class="fonts_title_presentation_main">
        Caos (χάος)<br>
        </span>
        </p>
    </div>


    <!----------------- SLIDE #1 ------------------>

        <div id = "slide1" class="step slide_PDVSA " data-x="1000" data-y="-1500">
            <p align="left">
            <span class="fonts_title_presentation_main">
            <br>  <br>
            Cosmogonía según Hesíodo / otras religiones
            <br><br>
            <video controls id="myvideo" width="420" height="340" controls loop>
                <source src="../../static/ref/videos/b1.mp4"></source>
            </video>


            </span>

            </p>
        </div>




    <!----------------- SLIDE #2 ------------------>

    <div id = "slide2" class="step slide_PDVSA" data-x="2000" data-y="-1500">
      <p align="left">
      <br><br>

      <span class="fonts_title_presentation_main">
      Teoría del caos<br><br>
      </span>
      - Henry Poncaire
      <br>
      - Edward Lorenz
      <br>
      - James York

      </p>
    </div>

  <!----------------- SLIDE #3 ------------------>
    <div id = "slide3" class="step slide_PDVSA" data-x="3000" data-y="-1500">
      <p align="left">
      <span class="fonts_content_presentation_main">
      <br><br>
      <strong>
      Teoría del caos<br>
      Entrevista con James Yorke
    </strong>
      </span>
      <br>
      <video controls id="myvideo" width="420" height="340" controls>
        <source src="../../static/ref/videos/001.mp4"></source>
    </video>
      </p>
      </div>


      <!----------------- SLIDE #5 ------------------>
        <div id = "slide5" class="step slide_PDVSA" data-x="5000" data-y="-1500">
          <p align="left">
          <br><br><br>

          <span class="fonts_title_presentation_main">
          Teoría del caos<br>
          Lectura recomendada:
          <br>
          </span>
          <br>
          <a href="../../static/ref/docs/casado.pdf"  target="_blank">
            Carlos Madrid Casado - Historia de la teoría del caos contada para escépticos
          </a>

          </p>
        </div>


        <!----------------- SLIDE #6 ------------------>
          <div id = "slide6" class="step slide_PDVSA" data-x="6000" data-y="-1500">
            <p align="left">
            <br><br><br><br>

            <span class="fonts_title_presentation_main">
            Complejidad y pensamiento complejo<br><br>
            </span>
            - Edgar Morín

            </p>
          </a>






            <br>
            </p>
          </div>

          <!----------------- SLIDE #7 ------------------>
            <div id = "slide7" class="step slide_PDVSA" data-x="7000" data-y="-1500">
              <p align="left">
              <span class="fonts_content_presentation_main">
              <br><br>
              <strong>
              Complejidad y pensamiento complejo<br>
              Pensamiento de complejidad (E. Morín)
            </strong>
              </span>
              <br>
              <video controls id="myvideo" width="420" height="340" controls>
                <source src="../../static/ref/videos/b3.mp4"></source>
            </video>
              </p>
          </div>

          <!----------------- SLIDE #8 ------------------>
            <div id = "slide8" class="step slide_PDVSA" data-x="8000" data-y="-1500">
              <p align="left">
              <br><br><br>

              <span class="fonts_title_presentation_main">
              Complejidad y pensamiento complejo<br>
              Lectura recomendada:
              <br>
              </span>
              <br>
              <a href="../../static/ref/docs/solana.pdf"  target="_blank">
                José Luis Solana - El pensamiento complejo de Edgar Morín. Críticas, incomprensiones y revisiones necesarias
              </a>

              </p>

          </div>

            <!----------------- SLIDE #9 ------------------>
            <div id = "slide9" class="step slide_PDVSA" data-x="9000" data-y="-1500">
              <p align="left">
              <br><br><br>

              <span class="fonts_title_presentation_main">
              Complejidad y pensamiento complejo<br>
              Lectura recomendada:
              <br>
              </span>
              <br>
              <a href="../../static/ref/docs/001.pdf"  target="_blank">
                Edgar Morín - Introducción al pensamiento complejo
              </a>

              </p>
          </div>

          <!----------------- SLIDE #10 ------------------>
          <div id = "slide10" class="step slide_PDVSA" data-x="10000" data-y="-1500">
            <p align="left">
            <br><br><br><br>

            <span class="fonts_title_presentation_main">
            Complejidad y pensamiento complejo<br>
            El pensamiento digital vs el pesamiento analógico
            <br>
            </span>
            <br>

            </p>
          </div>

        <!----------------- SLIDE #11 ------------------>
        <div id = "slide11" class="step slide_PDVSA" data-x="11000" data-y="-1500">
          <p align="left">
          <br><br><br><br>

          <span class="fonts_title_presentation_main">
          El post-modernismo / digi(meta)-modernismo
         </span>
          <br><br>
          - El paradigma científico
          <br>
          - El relativismo
          <br>
          - La flexibilización
          <br>
          - El cinismo y el sarcasmo
          <br>
          </p>
        </div>

        <!----------------- SLIDE #12 ------------------>
        <div id = "slide12" class="step slide_PDVSA" data-x="12000" data-y="-1500">
          <p align="left">
          <br><br><br><br>

          <span class="fonts_title_presentation_main">
          Caos, complejidad y post-postmodernismo. Transformación digital
         </span>
          <br><br>

          </p>
        </div>

        <!----------------- SLIDE #13 ------------------>
        <div id = "slide13" class="step slide_PDVSA" data-x="13000" data-y="-1500">
          <p align="left">
          <br><br><br><br><br>

          <span class="fonts_title_presentation_main">
          Revoluciones industriales
          <br>
          </span>
          <br>

          </p>
        </div>


        <!----------------- SLIDE #14 ------------------>
        <div id = "slide14" class="step slide_PDVSA" data-x="14000" data-y="-1500">

          <p align="left">
          <br><br><br><br>

          <span class="fonts_title_presentation_main">
          1 era revolución industrial (1760 - 1840 aprox)

         </span>
          <br><br>
          - Se origina en Gran Bretaña
          <br>
          - Aparece la máquina de vapor
          <br>
          - Cambio radical de la sociedad, política, economía y religiosa
          <br>
          - Doble revolución industrial
          <br>
          </p>
        </div>

        <!----------------- SLIDE #15 ------------------>
        <div id = "slide15" class="step slide_PDVSA" data-x="15000" data-y="-1500">
          <p align="left">
          <br><br>

          <span class="fonts_title_presentation_main">
          1 era revolución industrial (1760 - 1840 aprox)

         </span>
          <br><br>
          <strong>Ventajas y desventajas</strong><br><br>
                      <table style="width:95%">
                      <tr>
                          <td align="center" bgcolor="#cccc99">
                          <strong>  + </strong>
                          </td>
                          <td align="center" bgcolor="#ff4d4d">
                          <strong>  - </strong>
                          </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><br>
                        - Nuevos métodos de trabajo<br>
                        - Progreso Tecnológico<br>
                        - Nuevas formas de comercialización

                        </td>
                        <td align="left" valign="top">
                          <br>
                        - Nuevas clases sociales<br>
                        - Opresión e injusticias<br>
                        - Migraciones<br>
                        </td>
                    </tr>

                    </table>
          </p>
        </div>

        <!----------------- SLIDE #16 ------------------>
        <div id = "slide16" class="step slide_PDVSA" data-x="16000" data-y="-1500">

          <p align="left">
          <br><br><br>

          <span class="fonts_title_presentation_main">
          Adam Smith (1723-1790)
          <br><br>
         </span>
         - Nació en Escocia<br>
        - Primer economista moderno<br>
        - La Riqueza de las Naciones y las leyes naturales del mercado <br>
        - Economía libre, por intereses, individualista y competititiva <br><br>
        - <strong>Puntos clave:</strong> división del trabajo y la productividad (el alfiler)
        </p>
        </div>


        <!----------------- SLIDE #17 ------------------>
        <div id = "slide17" class="step slide_PDVSA" data-x="17000" data-y="-1500">
          <p align="left">
          <br><br><br>

          <span class="fonts_title_presentation_main">
          Adam Smith (1723-1790)
          <br>
         </span>
         <video controls id="myvideo" width="420" height="340" controls>
            <source src="../../static/ref/videos/008.mp4"></source>
        </video>
        </p>
        </div>



        <!----------------- SLIDE #18 ------------------>
        <div id = "slide18" class="step slide_PDVSA" data-x="18000" data-y="-1500">

          <p align="left">
          <br><br><br><br>

          <span class="fonts_title_presentation_main">
          2 da revolución industrial (1870 - 1945 aprox)

         </span>
          <br><br>
          - Más innovaciones, más tecnología
          <br> (sistemas de transporte, industria) y
                         nuevas fuentes de energía
          <br>
          - Aparece el capitalismo y la globalización
          <br>
          - Las dos guerras mundiales
          <br>
          - Explosión demográfica<br>
          <br>
          </p>

        </div>

        <!----------------- SLIDE #19 ------------------>
        <div id = "slide19" class="step slide_PDVSA" data-x="19000" data-y="-1500">

          <br><br><br>
                        <strong>Frederick Taylor y la gestión de procesos  </strong>
                        <br><br>

                        - Padre de la administración científica<br>
                        - Estudios del trabajo, el razonamiento científico aplicado al trabajo<br>
                        - Menos trabajo, menos esfuerzo y menos tiempo<br><br>
                         Otros: Fayol y Ford<br><br>


        </div>

        <!----------------- SLIDE #20 ------------------>
        <div id = "slide20" class="step slide_PDVSA" data-x="20000" data-y="-1500">
          <br><br><br>
                    <strong>
                     Resumen de Taylor y su importancia
                    </strong>
                    <br>
                    <video controls id="myvideo" width="420" height="340" controls>
                        <source src="../../static/ref/videos/009.mp4"></source>
                    </video>


        </div>


        <!----------------- SLIDE #21 ------------------>
        <div id = "slide21" class="step slide_PDVSA" data-x="21000" data-y="-1500">
          <br><br><br>
                    <strong>
                     Enfoque Taylorista (Modern Times)
                    </strong>
                    <br><br>
                    - Poca atención al elemento humano<br>
                    - Superespecialización del operario<br>
                    - Visión incompleta de la organización<br>
                    - Sistema cerrado y mecanicista

      </div>


<!----------------- SLIDE #22 ------------------>
        <div id = "slide22" class="step slide_PDVSA" data-x="22000" data-y="-1500">
          <p align="left">
          <br><br>

          <span class="fonts_title_presentation_main">
          2 da revolución industrial

         </span>
          <br><br>
          <strong>Ventajas y desventajas</strong><br><br>
                      <table style="width:95%">
                      <tr>
                          <td align="center" bgcolor="#cccc99">
                          <strong>  + </strong>
                          </td>
                          <td align="center" bgcolor="#ff4d4d">
                          <strong>  - </strong>
                          </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><br>
                        - Infraestructura y más tecnologías<br>
                        - Inventos  (carrera espacial)<br>
                        - Sociedad global y conectada

                        </td>
                        <td align="left" valign="top">
                          <br>
                        - Contaminación<br>
                        - Guerra fría y polarización<br>
                        - Violencia y pobreza<br>
                        </td>
                    </tr>

                    </table>
          </p>

        </div>
<!----------------- SLIDE #23 ------------------>

        <div id = "slide23" class="step slide_PDVSA" data-x="23000" data-y="-1500">
          <p align="left">
          <br><br><br><br>

          <span class="fonts_title_presentation_main">
          3 ra revolución industrial (1945 - ?)<br>
          Sociedad de la información (Castells, Toffler)

         </span>

          </p>
        </div>




<!----------------- SLIDE #24 ------------------>
        <div id = "slide24" class="step slide_PDVSA" data-x="24000" data-y="-1500">
          <p align="left">
          <br><br><br><br>

          <span class="fonts_title_presentation_main">
          3 ra revolución industrial (1945 - ?)<br>
          Desarrollo y conversación: características

         </span>

          </p>
        </div>

<!----------------- SLIDE #25 ------------------>
        <div id = "slide25" class="step slide_PDVSA" data-x="25000" data-y="-1500">
          <p align="left">
          <br><br><br><br><br>

          <span class="fonts_title_presentation_main">
            La información no es poder
            <br>
              El contenido no es el rey

         </span>

          </p>
        </div>

        <!----------------- SLIDE #26 ------------------>
                <div id = "slide26" class="step slide_PDVSA" data-x="26000" data-y="-1500">
                  <p align="left">
                  <br><br><br><br><br>

                  <span class="fonts_title_presentation_main">
                  4 ta revolución industrial ó industria 4.0 (??? -???)

                 </span>

                  </p>
                </div>


                <!----------------- SLIDE #27 ------------------>
                        <div id = "slide27" class="step slide_PDVSA" data-x="27000" data-y="-1500">
                          <p align="left">
                          <br><br><br><br>

                          <span class="fonts_title_presentation_main">
                          4 ta revolución industrial (industria 4.0)<br>
                          Lectura recomendada:
                          <br>
                          </span>
                          <br>
                          <a href="https://medium.com/metodolog%C3%ADas-%C3%A1giles-lima/transformaci%C3%B3n-digital-m%C3%A1s-caos-del-que-puedas-soportar-fed725c1cc92"  target="_blank">
                            Transformación Digital: Más caos del que puedas soportar
                          </a>

                          </p>
                        </div>


                        <!----------------- SLIDE #28 ------------------>
                                <div id = "slide28" class="step slide_PDVSA" data-x="28000" data-y="-1500">
                                  <p align="left">
                                  <br><br><br><br>

                                  <span class="fonts_title_presentation_main">
                                  4 ta revolución industrial (industria 4.0)<br>
                                  Lectura recomendada:
                                  <br>
                                  </span>
                                  <br>
                                  <a href="../../static/ref/docs/klaus.pdf"  target="_blank">
                                    Klaus Schwab - La cuarta revolución industrial
                                  </a>

                                  </p>
                                </div>


                                <!----------------- SLIDE #29 ------------------>
                                        <div id = "slide29" class="step slide_PDVSA" data-x="29000" data-y="-1500">
                                          <p align="left">
                                          <br><br><br>

                                          <span class="fonts_title_presentation_main">
                                          Las etapas de la disrupción de un negocio
                                          <br>
                                         </span>
                                         <video controls id="myvideo" width="420" height="340" controls>
                                            <source src="../../static/ref/videos/b4.mp4"></source>
                                        </video>
                                        </p>
                                        </div>


                                        <!----------------- SLIDE #30 ------------------>
                                                <div id = "slide30" class="step slide_PDVSA" data-x="30000" data-y="-1500">
                                                  <p align="left">
                                                  <br><br><br><br><br>

                                                  <span class="fonts_title_presentation_main">
                                                  La disrupción digital en Banplus
                                                  <br>
                                                  Nuevas oportunidades de negocio
                                                 </span>

                                                </p>
                                                </div>



                                                <!----------------- SLIDE #31 ------------------>
                                                        <div id = "slide31" class="step slide_PDVSA" data-x="31000" data-y="-1500">
                                                          <p align="left">
                                                          <br><br><br><br><br>

                                                          <span class="fonts_title_presentation_main">
                                                          La disrupción que se puede crear vs el negocio medular
                                                         </span>

                                                        </p>
                                                        </div>


          <!----------------- SLIDE #32 ------------------>
                  <div id = "slide32" class="step slide_PDVSA" data-x="32000" data-y="-1500">
                    <p align="left">
                    <br><br><br><br><br>

                    <span class="fonts_title_presentation_main">
                    Transformación digital como reestructurador de la dinámica organizacional
                   </span>

                  </p>
                  </div>


                  <!----------------- SLIDE #33 ------------------>
                          <div id = "slide33" class="step slide_PDVSA" data-x="33000" data-y="-1500">
                            <br><br><br>
                                      <strong>
                                       Ventajas de la transformación digital
                                      </strong>
                                      <br><br>
                                      - Genera experiencias y canales nuevos con el cliente<br>
                                      - Mejora la eficiencia operativa /reduccion de costos<br>
                                      - Generar nuevas fuentes de ingresos<br>
                                      - Mejora la colaboración interna y felicidad en los empleados.

                          </div>



                          <!----------------- SLIDE #34 ------------------>
                          <div id = "slide34" class="step slide_PDVSA" data-x="34000" data-y="-1500">
                            <br><br><br>
                                      <strong>
                                       Ventajas de la transformación digital
                                      </strong>
                                      <br><br>
                                      - Capacidad de respuesta rápida ante los cambios en el mercado<br>
                                      - Crear una ventaja competitiva para la organización<br>
                                      - Impulsa la cultura de la innovación dentro de la organización<br>
                                      - Profundiza el análisis de datos
                        </div>


                        <!----------------- SLIDE #35 ------------------>
                        <div id = "slide35" class="step slide_PDVSA" data-x="35000" data-y="-1500">
                          <br><br><br>
                                    <strong>
                                     ¿Qué NO es la transformación digital?
                                    </strong>
                                    <br><br>
                                    - CRM ó ERP<br>
                                    - Informatizar los procesos de la empresa<br>
                                    - Marketing digital y redes sociales<br>
                                    - Tienda online
                      </div>


                      <!----------------- SLIDE #36 ------------------>
                      <div id = "slide36" class="step slide_PDVSA" data-x="36000" data-y="-1500">
                        <br><br><br><br><br>
                                  <strong>
                                   Las fuentes de trabajo y la transformación digital
                                  </strong>
                                  <br><br>

                    </div>


                    <!----------------- SLIDE #37 ------------------>
                    <div id = "slide37" class="step slide_PDVSA" data-x="37000" data-y="-1500">
                      <br><br><br><br><br>
                                <strong>
                                 Herramientas de la transformación digital en banplus
                                 <br>
                                 ¿Con qué contamos?
                                </strong>
                                <br><br>
                    </div>


                    <!----------------- SLIDE #38 ------------------>
                    <div id = "slide38" class="step slide_PDVSA" data-x="38000" data-y="-1500">
                      <br><br><br><br><br>
                                <strong>
                                 Herramientas de la transformación digital en banplus
                                 <br>
                                 La gestión del conocimiento (KM) y el talento humano
                                </strong>
                                <br><br>Caso: XEROX
                                <br><br>
                    </div>

                    <!----------------- SLIDE #39 ------------------>
                    <div id = "slide39" class="step slide_PDVSA" data-x="39000" data-y="-1500">
                      <br><br><br><br>
                                <strong>
                                 Herramienta técnica: El big data en el sector bancario:
                                </strong>
                                <br><br>
                                - Experiencia del cliente<br>
                                - Gestón del riesgo<br>
                                - Gestión de fraudes<br>
                    </div>


                    <!----------------- SLIDE #40 ------------------>
                    <div id = "slide40" class="step slide_PDVSA" data-x="40000" data-y="-1500">
                      <br><br><br><br>
                                <strong>
                                  La Transformación digital como proceso disruptivo

                                </strong>
                                <br><br>
                                Para que un proceso de Transformación Digital tenga éxito, la primera en digitalizarse ha de ser la junta directiva.
                    </div>


                    <!----------------- SLIDE #41 ------------------>
                    <div id = "slide41" class="step slide_PDVSA" data-x="41000" data-y="-1500">
                      <br><br><br><br>
                                <strong>
                                  Herramienta técnica: Intranet orientada a servicios

                                </strong>
                                <br><br>
                                - Colaboración vs silos
                                <br>
                                - Bienestar (Niko-Niko y el M3.0)
                                <br><br>
                                <a href="https://management30.com/blog/team-engagement-niko-niko-calendar/"  target="_blank">
                                  A template
                                </a>
                    </div>


                    <!----------------- SLIDE #42 ------------------>
                    <div id = "slide42" class="step slide_PDVSA" data-x="42000" data-y="-1500">
                      <br><br><br><br><br>
                                <strong>
                                  Herramienta técnica: Microservicios & APIs

                                </strong>

                    </div>


                    <!----------------- SLIDE #43 ------------------>
                    <div id = "slide43" class="step slide_PDVSA" data-x="43000" data-y="-1500">
                      <br><br><br><br><br>
                                <strong>
                                  Control de versiones: Management 1.0 al 3.0

                                </strong>
                    </div>


                    <!----------------- SLIDE #44 ------------------>
                    <div id = "slide44" class="step slide_PDVSA" data-x="44000" data-y="-1500">
                      <br><br><br><br><br>
                                <strong>
                                  Control de versiones: Web 1.0 a la 4.0

                                </strong>
                    </div>


                    <!----------------- SLIDE #45 ------------------>
                    <div id = "slide45" class="step slide_PDVSA" data-x="45000" data-y="-1500">

                      <br><br><br><br><br>
                                <strong>
                                  Control de versiones: Enterprise 1.0 a la 3.0      </strong>
                                <br><br>  Lectura recomendada:
                                  <br>
                                  </span>
                                  <br>
                                  <a href="https://medium.com/@icommunis/7-things-you-need-to-know-about-enterprise-3-0-a3aca3c9ee34"  target="_blank">
                                    7 things you need to know about Enterprise 3.0
                                  </a>



                    </div>

                    <!----------------- SLIDE #46 ------------------>
                    <div id = "slide46" class="step slide_PDVSA" data-x="46000" data-y="-1500">
                      <p align="left">
  <span class="fonts_content_presentation_main"></span>
  <br>
  <strong>RRHH juega un rol clave en la transformación digital</label></strong>
  <br>
  <br>
  <a href="../../static/ref/docs/HRGV.pdf" target="_blank"><img src="../../static/ref/images/005c.jpg" height="400" width="800"></a>

                    </div>


                    <!----------------- SLIDE #47 ------------------>
                    <div id = "slide47" class="step slide_PDVSA" data-x="47000" data-y="-1500">
                      <br><br><br><br><br>
                                <strong>
                                  Herramienta metodológica/organizacional: Marco ágil
                                  <br>
                                  Yo agrego: estoicismo

                                </strong>
                    </div>

                    <!----------------- SLIDE #48 ------------------>
                    <div id = "slide48" class="step slide_PDVSA" data-x="48000" data-y="-1500">
                      <span class="fonts_content_presentation_main"></span>
          <br>
          <strong>'Scrum', 'agile'... <br>así son las nuevas formas de trabajo en BBVA</strong>
          <br><br>
          <video controls id="myvideo" width="420" height="340" controls>
              <source src="../../static/ref/videos/002.mp4"></source>
          </video>
                    </div>


                    <!----------------- SLIDE #49 ------------------>
                    <div id = "slide49" class="step slide_PDVSA" data-x="49000" data-y="-1500">
                      <p align="left">
        <span class="fonts_content_presentation_main"></span>
        <br>
        <strong>Las grandes frustaciones de: <a href="https://www.wikiwand.com/es/Scrum_(desarrollo_de_software)" target="_blank" class="fonts_main_links"> Jeff Sutherland</a></strong>
        <br><br>
        <img src="../../static/ref/images/003.jpg" width="700" height="300">
        <br>
        La grandeza no puede ser impuesta; debe venir de adentro. Pero vive dentro de todos nosotros
        Lectura recomendada:
        <a href="../../static/ref/docs/007.pdf" target="_blank" class="fonts_main_links"><br>
        The Chaos Report
      </a>
                    </div>

                    <!----------------- SLIDE #50 ------------------>
                    <div id = "slide50" class="step slide_PDVSA" data-x="50000" data-y="-1500">
                      <p align="left">
        <br>
      <span class="fonts_content_presentation_main">

      <strong>Gestionar la complejidad con Scrum. Por Jerónimo Palacios</strong>
      <a href="../../static/ref/docs/002.pdf" target="_blank"><img src="../../static/ref/images/002.png" width="659" height="422"></a>
      </span>
      <br>
      </p>

                    </div>


                    <!----------------- SLIDE #50 ------------------>
                    <div id = "slide51" class="step slide_PDVSA" data-x="51000" data-y="-1500">
                      <p align="left">
      <span class="fonts_content_presentation_main">
      <br>
      <strong>Metodologías ágiles: tendencia y falacia?<br>Un poco de escepticismo positivo</strong>
      <a href="https://hrdailyadvisor.blr.com/2018/02/22/agile-fail-top-challenge-hr-teams-2018/" target="_blank"><img src="../../static/ref/images/002a.png" width="659" height="422"></a>
      </span>
      <br>
      </p>

                    </div>


                    <!----------------- SLIDE #50 ------------------>
                    <div id = "slide52" class="step slide_PDVSA" data-x="52000" data-y="-1500">
                      <br>
        <strong>Lecciones de management: Steve Jobs (Apple)</strong>
        <br>
        Las reuniones cortas son importantes: sprint meettings.
        <br><br>

        <video controls id="myvideo" width="420" height="340" controls>
            <source src="../../static/ref/videos/005.mp4">
              Tu navegador no implementa el elemento <code>video</code>.
        </video>


                    </div>


                    <!----------------- SLIDE #50 ------------------>
                    <div id = "slide53" class="step slide_PDVSA" data-x="53000" data-y="-1500">
                      <p align="left">
      <span class="fonts_content_presentation_main"></span>
      <br>
      <strong>Cómo sobrevivir?</label></strong>
      <br>
      A través de un enfoque pragmático?
      el pragmatismo suele ser asociado a la practicidad... y las bases teóricas?
      <br><br>
      <table>

          <tr>
          <td><a href="https://www.wikiwand.com/es/John_Dewey" target="_blank"><img src="../../static/ref/images/006a.jpg" height="270" width="220"></a></td>
          <td><a href="https://www.wikiwand.com/es/Charles_Sanders_Peirce" target="_blank"><img src="../../static/ref/images/006b.jpg" height="270" width="220"></a></td>
          <td><a href="https://www.wikiwand.com/es/William_James" target="_blank"><img src="../../static/ref/images/006c.jpg" height="270" width="220"></a></td>
          </tr>
      </table>

                    </div>


                    <!----------------- SLIDE #50 ------------------>
                    <div id = "slide54" class="step slide_PDVSA" data-x="54000" data-y="-1500">
                      <br>
      <p align="left">
      <span class="fonts_content_presentation_main"></span>
      <br>
      <strong>Críticas al pragmatismo</label></strong>

      <br>
      <a href="http://www.filosofia.org/hem/dep/rcf/n01p024.htm" class="fonts_main_links">Filosofia.org de Cuba </a><br>
      <a href="https://filosofiaparaprofanos.com/2014/10/07/un-politico-pragmatico/" class="fonts_main_links">Filosofía para profanos </a>
<br><br>
        Dejemos de lado el idealismo y vamos a centrarnos en el pragmatismo:
        ¿Cuánto tenemos que invertir para modernizar Banplus?”
        <br>
        <br>


                    </div>


                    <!----------------- SLIDE #50 ------------------>
                    <div id = "slide55" class="step slide_PDVSA" data-x="55000" data-y="-1500">
                      <br>
                    <strong>Control empírico e incremental</strong>
                    <br><br>
                    <img src="../../static/ref/images/025.gif" width="550" height="380">

                  </div>
                  <!----------------- SLIDE #66 ------------------>
                  <div id = "slide56" class="step slide_PDVSA" data-x="56000" data-y="-1500">
                    <br>
                    <strong>Control empírico e incremental (detalle)</strong>

                    <br><br>
                    <img src="../../static/ref/images/026.gif" width="550" height="380">


                    </div>

                    <!----------------- SLIDE #50 ------------------>
                    <div id = "slide57" class="step slide_PDVSA" data-x="57000" data-y="-1500">
                      <br>
                  <strong> Shewhart / Deming</strong>
                  <br><br>

                  Las mejoras contínuas


                  <br><br>
                  <img src="../../static/ref/images/027.jpg" width="260" height="260">



                  </a>


                    </div>

                    <!----------------- SLIDE #50 ------------------>
                    <div id = "slide58" class="step slide_PDVSA" data-x="58000" data-y="-1500">
                      <br><br>
              <strong>La agilidad apuesta a:</strong>

              <br><br>

              - Un modelo horizontal<br>
              - Las decisiones no pasan por muchos niveles de autoridad<br>
              - Controles de Calidad<br>
              - Modelos sin jefe, líderes en trabajadores<br>
              - Valorar la experiencia, individualidad e ideas<br>
              - Resolver problemas<br>
              - Tener: misión, visión, valores, cultura organizacionals

                    </div>

                    <!----------------- SLIDE #50 ------------------>
                    <div id = "slide59" class="step slide_PDVSA" data-x="59000" data-y="-1500">
                      <br>
                      Es esto Anarquismo? Anomia? Bakunin? Von Misses?  DIY?<br>
                      Punks? Comunismo? Igualdad? Ayn Rand y el altruismo?
                      <br><br>
                      <img src="../../static/ref/images/012a.jpg" height="200" width="200">
                      <br><br>
                      Lecturas recomendadas:
                      <br>
                      <a href="http://devmethodologies.blogspot.com/2015/02/scrum-communism.html" target="_blank" class="fonts_main_links">
                          Scrum == Communism ?
                        </a><br>
                      <a href="https://agileanarchy.wordpress.com/scrum-a-new-way-of-thinking/" target="_blank" class="fonts_main_links">
                          Agile Anarchy
                        </a>

                    </div>

                    <!----------------- SLIDE #50 ------------------>
                    <div id = "slide60" class="step slide_PDVSA" data-x="60000" data-y="-1500">
                      <br>  <br>  <br>  <br>  <br>
                                      <strong>Estructuras verticales u horizontales </strong><br>
                                        (no todo es emancipación)
                                    </div>


                                    <!----------------- SLIDE #30 ------------------>
                                            <div id = "slide30" class="step slide_PDVSA" data-x="30000" data-y="-1500">
                                              <br>  <br>
                                              <strong>Organizaciones verticales </strong><br><br><br>
                                                <table style="width:95%">

                                                <tr>
                                                  <td align="center" bgcolor="#cccc99">
                                                  <strong>  + </strong>
                                                  </td>
                                                  <td align="center" bgcolor="#ff4d4d">
                                                  <strong>  - </strong>
                                                  </td>
                                                </tr>

                                                <tr>
                                                  <td align="left" valign="top"><br>
                                                    - Control<br>
                                                    - Reparto de tareas<br>
                                                    - Especialización (T, F)<br><br>

                                                  </td>
                                                  <td align="left" valign="top">
                                                    - Burocracia<br>
                                                    - Coste de salarios<br>
                                                    - Pérdida de oportunidades<br>
                                                    - Desmotivación <br>y frustaciones
                                                  </td>
                                                </tr>

                                                  </table>

                    </div>

                    <!----------------- SLIDE #50 ------------------>
                    <div id = "slide61" class="step slide_PDVSA" data-x="61000" data-y="-1500">
                      <br>  <br>
                                                      <strong>Organizaciones horizontales </strong><br><br><br>
                                                        <table style="width:95%">

                                                        <tr>
                                                          <td align="center" bgcolor="#cccc99">
                                                          <strong>  + </strong>
                                                          </td>
                                                          <td align="center" bgcolor="#ff4d4d">
                                                          <strong>  - </strong>
                                                          </td>
                                                        </tr>

                                                        <tr>
                                                          <td align="left" valign="top">
                                                            - Satisfacción por la autonomía<br>
                                                            - Cooperación y colaboración<br>
                                                            - Agilidad y adaptación
                                                          </td>
                                                          <td align="left" valign="top">
                                                            - Superposición y solapamiento <br>
                                                            - Escasa promoción<br>
                                                            - Descontrol
                                                          </td>
                                                        </tr>

                                                          </table>

                    </div>

                    <div id = "slide62" class="step slide_PDVSA" data-x="62000" data-y="-1500">
                      <br>
                      <strong>5 formas de usar AI en RRHH</strong>
                      <br><br>
                      <a href="http://bigdata-madesimple.com/5-ways-to-use-artificial-intelligence-ai-in-human-resources/" target="_blank">
                      <img src="../../static/ref/images/020.jpg" width="750" height="400">
                    </a>


                    </div>

                    <div id = "slide63" class="step slide_PDVSA" data-x="63000" data-y="-1500">
                      <br>
                      <strong>Agile + AI + RRHH</strong>
                      <br><br>
                      <a href="https://www.employeeconnect.com/blog/artificial-intelligence-hr/" target="_blank">
                      <img src="../../static/ref/images/021.jpg" width="750" height="400">
                    </a>


                    </div>

                    <div id = "slide64" class="step slide_PDVSA" data-x="64000" data-y="-1500">
                      <br><br><br><br>
              <strong> Be Agile...</strong>

                    </div>


<div class="hint">
    <p>Presione espaciadora</p>
</div>
<script>
    if ("ontouchstart" in document.documentElement) {
        document.querySelector(".hint").innerHTML = "<p>Presione las flechas izquierda y/o derecha para retroceder/avanzar</p>";
    }
</script>


<script>impress().init();</script>

</div>

    <div class="footer_main">

        <?php include '../includes/footer_presentation.html';?>
    </div>



</body>
</html>
