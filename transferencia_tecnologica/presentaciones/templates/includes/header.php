
<div class="header_GBV">
    <img src="../../static/images/logo-GBV.jpg" class="logo_GBV" />
</div>
***

<nav class="navbar_bootstrap_PDVSA navbar_inverse_bootstrap_PDVSA">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle_PDVSA" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img src="../../static/images/logo-PDVSA-CVP.png" class="logo_PDVSA"/>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav_PDVSA">
                <li><a href="#">Ayuda</a></li>
                <li><a href="#">Acerca de esta aplicación</a></li>
            </ul>
        </div>
    </div>
</nav>
